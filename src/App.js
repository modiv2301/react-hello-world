import Footer from "./Footer";
import Header from "./Header";
// Component
function App() {
    return (
        <div>
            <Header />
            <div>This is main page</div>
            {/* reuseiblity */}
            <Footer title="footer" name="Vivek" karan="sharma" />
            <Footer title="footer1" name="Raj" />
            <Footer title="footer2" name="Rahul" />
            <Footer title="footer3" name="Karan" />
            <Footer title="footer4" name="Tanuj" />
        </div>
    )
}


export default App;